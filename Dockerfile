# Use the SDK image for building and running migrations
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["ContactManagement.csproj", "./"]
RUN dotnet restore "./ContactManagement.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "ContactManagement.csproj" -c Release -o /app/build
RUN dotnet publish "ContactManagement.csproj" -c Release -o /app/publish

# Final stage to run the application
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS final
WORKDIR /app

# Copy the application files from the build stage
COPY --from=build /app/publish .

# Create the /keys directory and set permissions. 777 significa que proprietário, grupo e usuário têm permissão de leitura, escrita e execução. 700 somente proprietário. leitura (4), escrita (2) e execução (1)  
RUN mkdir -p /keys && chmod -R 777 /keys

# Entrypoint to run the application directly
ENTRYPOINT ["dotnet", "ContactManagement.dll"]
