using Microsoft.AspNetCore.Mvc;
using ContactManagement.Models;
using ContactManagement.Data;
using Microsoft.EntityFrameworkCore;
using MySqlConnector;
using Microsoft.AspNetCore.Authorization;

namespace ContactManagement.Controllers
{
    public class ContactsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<ContactsController> _logger;

        public ContactsController(ApplicationDbContext context, ILogger<ContactsController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: Contacts
        [HttpGet]
        [AllowAnonymous]

        public async Task<IActionResult> Index()
        {
            _logger.LogInformation("Contacts Index action called.");
            return View(await _context.Contacts.Where(c => !c.IsDeleted).ToListAsync());
        }

        // GET: Contacts/Details/5
        [HttpGet]
        [AllowAnonymous]

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = await _context.Contacts
                .FirstOrDefaultAsync(m => m.ID == id && !m.IsDeleted);
            if (contact == null)
            {
                return NotFound();
            }

            return View(contact);
        }

        // GET: Contacts/Create
        //[Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Contacts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize]
        public async Task<IActionResult> Create([Bind("ID,Name,ContactNumber,Email")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Add(contact);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException ex)
                {
                    if (ex.InnerException is MySqlException sqlEx && (sqlEx.Number == 1062 || sqlEx.Message.Contains("Duplicate entry")))
                    {
                        ModelState.AddModelError("", "A contact with the same contact number or email already exists.");
                    }
                    else
                    {
                        ModelState.AddModelError("", "An error occurred while saving the contact. Please try again.");
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "Validation error. Please check your input and try again.");
            }
            return View(contact);
        }

        // GET: Contacts/Edit/5
        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = await _context.Contacts.FindAsync(id);
            if (contact == null || contact.IsDeleted)
            {
                return NotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,ContactNumber,Email")] Contact contact)
        {
            if (id != contact.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contact);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContactExists(contact.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                catch (DbUpdateException ex)
                {
                    if (ex.InnerException is MySqlException sqlEx && (sqlEx.Number == 1062 || sqlEx.Message.Contains("Duplicate entry")))
                    {
                        ModelState.AddModelError("", "A contact with the same contact number or email already exists.");
                    }
                    else
                    {
                        ModelState.AddModelError("", "An error occurred while updating the contact. Please try again.");
                    }
                }
            }
            return View(contact);
        }

        // GET: Contacts/Delete/5
        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = await _context.Contacts
                .FirstOrDefaultAsync(m => m.ID == id && !m.IsDeleted);
            if (contact == null)
            {
                return NotFound();
            }

            return View(contact);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        //[Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contact = await _context.Contacts.FindAsync(id);
            if (contact != null)
            {
                contact.IsDeleted = true;
                _context.Contacts.Update(contact);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction(nameof(Index));
        }

        private bool ContactExists(int id)
        {
            return _context.Contacts.Any(e => e.ID == id);
        }
    }
}
