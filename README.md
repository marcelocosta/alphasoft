# Contact Management Application

## Instruções de Instalação

Baixar Docker Desktop, instalar e executar.

1. Clone o repositório:
   ```sh
   git clone <repository-url>
   cd ContactManagement

2. Comandos para execução:

dotnet restore
dotnet build
docker compose up
