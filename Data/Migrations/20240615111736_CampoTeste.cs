﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ContactManagement.Data.Migrations
{
    public partial class CampoTeste : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MeuTeste",
                table: "Contacts",
                type: "longtext",
                nullable: false)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MeuTeste",
                table: "Contacts");
        }
    }
}
