﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ContactManagement.Data.Migrations
{
    public partial class TesteMigra : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TesteMigra",
                table: "Contacts",
                type: "longtext",
                nullable: false)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TesteMigra",
                table: "Contacts");
        }
    }
}
