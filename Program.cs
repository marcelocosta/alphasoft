using ContactManagement.Data;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MySqlConnector;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseMySql(builder.Configuration.GetConnectionString("MariaDB"),
    new MySqlServerVersion(new Version(10, 6)),
    mySqlOptions => mySqlOptions.EnableRetryOnFailure()));

builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<ApplicationDbContext>()
    .AddDefaultTokenProviders();

builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();

/* builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = $"/Identity/Account/Login";
    options.LogoutPath = $"/Identity/Account/Logout";
}); */

builder.Services.AddDataProtection()
    .PersistKeysToFileSystem(new DirectoryInfo(@"/keys"))
    .SetApplicationName("ContactManagement");

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}
else
{
    app.UseDeveloperExceptionPage();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapRazorPages();

// Aplicar migrações no início
using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    var dbContext = services.GetRequiredService<ApplicationDbContext>();
    var logger = services.GetRequiredService<ILogger<Program>>();

    try
    {
        dbContext.Database.Migrate();

        // Criar usuário administrador estático
        var userManager = services.GetRequiredService<UserManager<IdentityUser>>();
        var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
        SeedAdminUser(userManager, roleManager, logger).Wait();

        // Código temporário para redefinir a senha do administrador
        var adminUser = userManager.FindByEmailAsync("admin@teste.com").Result;
        if (adminUser != null)
        {
            var token = userManager.GeneratePasswordResetTokenAsync(adminUser).Result;
            var result = userManager.ResetPasswordAsync(adminUser, token, "Admin@123").Result;

            if (result.Succeeded)
            {
                logger.LogInformation("Senha redefinida com sucesso!");

                // Teste de login
                var signInManager = services.GetRequiredService<SignInManager<IdentityUser>>();
                var loginResult = signInManager.PasswordSignInAsync("admin@teste.com", "Admin@123", isPersistent: false, lockoutOnFailure: false).Result;

                if (loginResult.Succeeded)
                {
                    logger.LogInformation("Login teste bem-sucedido para admin@teste.com.");
                }
                else
                {
                    if (loginResult.IsLockedOut)
                    {
                        logger.LogError("Falha no teste de login: Usuário está bloqueado.");
                    }
                    else if (loginResult.IsNotAllowed)
                    {
                        logger.LogError("Falha no teste de login: Login não permitido.");
                    }
                    else if (loginResult.RequiresTwoFactor)
                    {
                        logger.LogError("Falha no teste de login: Requer autenticação de dois fatores.");
                    }
                    else
                    {
                        logger.LogError("Falha no teste de login para admin@teste.com. Verifique as credenciais e a configuração.");
                    }
                }
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    logger.LogError($"Erro ao redefinir a senha: {error.Description}");
                }
            }
        }
        else
        {
            logger.LogError("Não foi possível encontrar o usuário admin@teste.com para redefinir a senha.");
        }
    }
    catch (Exception ex)
    {
        logger.LogError(ex, "Ocorreu um erro ao aplicar migrações no banco de dados.");
    }
}

app.Run();

async Task SeedAdminUser(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager, ILogger logger)
{
    var adminRole = "Admin";

    if (!await roleManager.RoleExistsAsync(adminRole))
    {
        await roleManager.CreateAsync(new IdentityRole(adminRole));
    }

    var adminUserName = "admin";
    var adminEmail = "admin@teste.com";
    var adminUser = await userManager.FindByNameAsync(adminUserName);

    if (adminUser == null)
    {
        adminUser = new IdentityUser
        {
            UserName = adminUserName,
            Email = adminEmail,
            EmailConfirmed = true
        };
        //senha deve ter pelo menos um maiúsculo
        var createUserResult = await userManager.CreateAsync(adminUser, "Admin@123");

        if (createUserResult.Succeeded)
        {
            await userManager.AddToRoleAsync(adminUser, adminRole);
        }
        else
        {
            foreach (var error in createUserResult.Errors)
            {
                logger.LogError($"Erro ao criar o usuário: {error.Description}");
            }
        }
    }
    else
    {
        logger.LogInformation("Usuário admin já existe.");
    }
}