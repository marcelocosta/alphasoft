using System.ComponentModel.DataAnnotations;

namespace ContactManagement.Models
{
    public class Contact
    {
        public int ID { get; set; }

        [Required]
        [MinLength(5)]
        public string Name { get; set; }

        [Required]
        [RegularExpression(@"^\d{9}$", ErrorMessage = "Contact number must be 9 digits")]
        public string ContactNumber { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public bool IsDeleted { get; set; }
    }
}
